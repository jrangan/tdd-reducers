function createBasicReducer() {
  return (state, action) => {
    if (action.type === "INCREMENT") {
      return { value: 1 };
    }
    return { value: 0 };
  };
}

export function reducerFactory(initialState, actionHandlers) {
  const reducer = (state, action) => {
    const actionHandler = actionHandlers[action.type];
    if (state === undefined) {
      state = initialState;
    }
    if (actionHandler === undefined) return state;
    return actionHandler(state, action);
  };
  return reducer;
}

export const combineReducers = (reducerSpecification) => (state, action) =>
  Object.entries(reducerSpecification).reduce(
    (updatedState, [reducerKey, reducer]) => ({
      ...updatedState,
      [reducerKey]: reducer(
        state === undefined ? undefined : state[reducerKey],
        action
      ),
    }),
    {}
  );

export default createBasicReducer;
