import createBasicReducer, { reducerFactory, combineReducers } from "./reducer";

describe("reducer", () => {
  describe("createBasicReducer", () => {
    it("should return the initial state when state is undefined", () => {
      const basicReducer = createBasicReducer();
      expect(basicReducer(undefined, {})).toEqual({ value: 0 });
    });

    it("should handle INCREMENT action", () => {
      const basicReducer = createBasicReducer();
      expect(basicReducer({ value: 0 }, { type: "INCREMENT" })).toEqual({
        value: 1,
      });
    });
  });

  describe("reducerFactory", () => {
    describe("given an initial state and an action handler", () => {
      let initialState;
      let actionHandlers;
      let reducer;
      beforeEach(() => {
        initialState = { value: 0 };
        actionHandlers = {
          INCREMENT: (state) => ({ value: state.value + 1 }),
        };
        reducer = reducerFactory(initialState, actionHandlers);
      });

      describe("and the reducer is given an undefined state", () => {
        describe("and an undefined action", () => {
          it("should return the initial state", () => {
            expect(reducer(undefined, {})).toEqual(initialState);
          });
        });
        describe("and an action", () => {
          it("should return the updated state", () => {
            expect(reducer(undefined, { type: "INCREMENT" })).toEqual({
              value: 1,
            });
          });
        });
      });

      describe("and the reducer is given the initial state", () => {
        describe("and an undefined action", () => {
          it("should return the state", () => {
            expect(reducer(initialState, {})).toEqual(initialState);
          });
        });
        describe("and an action", () => {
          it("should return the updated state", () => {
            expect(reducer(initialState, { type: "INCREMENT" })).toEqual({
              value: 1,
            });
          });
        });
      });

      describe("and the reducer is given the state", () => {
        describe("and an undefined action", () => {
          it("should return the state", () => {
            expect(reducer({ value: 99 }, {})).toEqual({ value: 99 });
          });
        });
        describe("and an action", () => {
          it("should return the updated state", () => {
            expect(reducer({ value: 99 }, { type: "INCREMENT" })).toEqual({
              value: 100,
            });
          });
        });
      });
    });

    describe("given initial state and multiple action handlers", () => {
      let initialState;
      let actionHandlers;
      let reducer;
      beforeEach(() => {
        initialState = { value: 0 };
        actionHandlers = {
          INCREMENT: (state) => ({ value: state.value + 1 }),
          DECREMENT: (state) => ({ value: state.value - 1 }),
        };
        reducer = reducerFactory(initialState, actionHandlers);
      });

      it("should dispatch to the correct handler", () => {
        expect(reducer(initialState, { type: "INCREMENT" })).toEqual({
          value: 1,
        });
        expect(reducer(initialState, { type: "DECREMENT" })).toEqual({
          value: -1,
        });
      });
    });

    describe("given an initial state and an action handler that expects an action with a payload", () => {
      let initialState;
      let actionHandlers;
      let reducer;
      beforeEach(() => {
        initialState = { message: "" };
        actionHandlers = {
          SET_MESSAGE: (state, action) => {
            const message = action.payload;
            return { message };
          },
        };
        reducer = reducerFactory(initialState, actionHandlers);
      });

      describe("and the reducer is given an undefined state", () => {
        describe("and an action with a payload", () => {
          it("should return the updated state", () => {
            expect(
              reducer(undefined, { type: "SET_MESSAGE", payload: "Hello" })
            ).toEqual({
              message: "Hello",
            });
          });
        });
      });

      describe("and the reducer is given the initial state", () => {
        describe("and an action with a payload", () => {
          it("should return the updated state", () => {
            expect(
              reducer(initialState, { type: "SET_MESSAGE", payload: "Hello" })
            ).toEqual({
              message: "Hello",
            });
          });
        });
      });

      describe("and the reducer is given the state", () => {
        describe("and an action with a payload", () => {
          it("should return the updated state", () => {
            expect(
              reducer(
                { message: "Old Message" },
                { type: "SET_MESSAGE", payload: "Hello" }
              )
            ).toEqual({
              message: "Hello",
            });
          });
        });
      });
    });
  });

  describe("combineReducers", () => {
    test("should create a combined reducer using reducerFactory", () => {
      const initialState = {
        counter: 0,
        message: "",
        pezHitz: 0,
        favouriteMovies: {
          action: ["Last Action Hero"],
          horror: ["Nightmare on Elm Street", "Leprechaun"],
        },
      };

      const counterHandlers = {
        INCREMENT: (state) => state + 1,
        DECREMENT: (state) => state - 1,
      };

      const messageHandlers = {
        SET_MESSAGE: (state, action) => action.payload,
      };

      const pezHitzHandlers = {
        TAKE_HIT: (state) => state + 1,
      };

      const actionMovieHandlers = {
        ADD_ACTION_MOVIE: (state, action) => [...state, action.payload],
      };

      const horrorMovieHandlers = {
        ADD_HORROR_MOVIE: (state, action) => [...state, action.payload],
      };

      const counterReducer = reducerFactory(
        initialState.counter,
        counterHandlers
      );

      const messageReducer = reducerFactory(
        initialState.message,
        messageHandlers
      );

      const pezHitzReducer = reducerFactory(
        initialState.pezHitz,
        pezHitzHandlers
      );

      const actionMovieReducer = reducerFactory(
        initialState.favouriteMovies.action,
        actionMovieHandlers
      );

      const horrorMovieReducer = reducerFactory(
        initialState.favouriteMovies.horror,
        horrorMovieHandlers
      );

      const favouriteMoviesReducer = combineReducers({
        action: actionMovieReducer,
        horror: horrorMovieReducer,
      });

      const rootReducer = combineReducers({
        counter: counterReducer,
        message: messageReducer,
        pezHitz: pezHitzReducer,
        favouriteMovies: favouriteMoviesReducer,
      });

      expect(rootReducer).toBeInstanceOf(Function);
      const testState = rootReducer(undefined, {});

      expect(testState).toEqual(initialState);

      expect(rootReducer(testState, { type: "INCREMENT" })).toEqual({
        counter: 1,
        message: "",
        pezHitz: 0,
        favouriteMovies: {
          action: ["Last Action Hero"],
          horror: ["Nightmare on Elm Street", "Leprechaun"],
        },
      });
      expect(rootReducer(testState, { type: "DECREMENT" })).toEqual({
        counter: -1,
        message: "",
        pezHitz: 0,
        favouriteMovies: {
          action: ["Last Action Hero"],
          horror: ["Nightmare on Elm Street", "Leprechaun"],
        },
      });

      expect(rootReducer(testState, { type: "TAKE_HIT" })).toEqual({
        counter: 0,
        message: "",
        pezHitz: 1,
        favouriteMovies: {
          action: ["Last Action Hero"],
          horror: ["Nightmare on Elm Street", "Leprechaun"],
        },
      });

      expect(
        rootReducer(testState, {
          type: "ADD_ACTION_MOVIE",
          payload: "Die Hard",
        })
      ).toEqual({
        counter: 0,
        message: "",
        pezHitz: 0,
        favouriteMovies: {
          action: ["Last Action Hero", "Die Hard"],
          horror: ["Nightmare on Elm Street", "Leprechaun"],
        },
      });

      expect(
        rootReducer(testState, {
          type: "SET_MESSAGE",
          payload: "Hello, world!",
        })
      ).toEqual({
        counter: 0,
        message: "Hello, world!",
        pezHitz: 0,
        favouriteMovies: {
          action: ["Last Action Hero"],
          horror: ["Nightmare on Elm Street", "Leprechaun"],
        },
      });
    });
  });
});
